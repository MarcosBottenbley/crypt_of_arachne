﻿using UnityEngine;
using System.Collections;

public class healthBar : MonoBehaviour {

	SpriteRenderer sr;

	// Use this for initialization
	void Start () {
		sr = GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.localScale.x < 0.5f) {
			sr.color = Color.red;
		} else {
			sr.color = Color.green;
		}

		if (transform.localScale.x < 0f) {
			transform.localScale = Vector3.zero;
		}
	}

	public void loseHealth(int amount){
		transform.localScale -= new Vector3(0.1f * amount, transform.localScale.y, transform.localScale.z);
	}

	public void gainHealth(int amount) {
		transform.localScale += new Vector3 (0.1f * amount, transform.localScale.y, transform.localScale.z);
	}

	public void resetHealth() {
		transform.localScale = new Vector3 (1f, transform.localScale.y, transform.localScale.z);
	}
}
