﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CrateScript : MonoBehaviour
{
    private Rigidbody2D rb;

    // the dropped object
    public GameObject drop;

	//these are dumb but i don't know how to access prefabs from code
	//TODO: tell david to change this
	public GameObject drop1;
	public GameObject drop2;
	public GameObject drop3;

    public int health;
    public int value;

    private bool otherTarget;

    public GameObject text;
    private GameObject generatedText;

    public string powerupText = "";

    // Use this for initialization
    void Start()
    {
		setCrateValues ();

		Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);

        GameObject canvas = GameObject.Find("Canvas");
        generatedText = (GameObject) Instantiate(text, pos, Quaternion.identity);
        
        generatedText.GetComponent<Text>().text = powerupText + "\n\n" + value + " GP";
        generatedText.transform.SetParent(canvas.transform);
    }

    // Update is called once per frame
    void Update()
    {
        generatedText.transform.position = Camera.main.WorldToScreenPoint(transform.position);
        if (health <= 0)
        {
            die();
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        AttackLength attack = other.GetComponent<AttackLength>();
        PlayerControl player = attack.gameObject.GetComponentInParent<PlayerControl>();

        if (other.tag == "Sword")
        {
            //AttackLength attack = other.GetComponent<AttackLength>();
            if (attack != null && attack.getTimer() <= .05f && playerHasEnoughGold(player))
            {
                health -= 1;

                if (health <= 0)
                {
                    player.purchaseCrate(value);
                }
            }
        }
    }

    //Kills spider object and drops gold.
    public void die()
    {
        Instantiate(drop, transform.position, transform.rotation);
        Destroy(gameObject);
        Destroy(generatedText.gameObject);
    }

    public bool playerHasEnoughGold(PlayerControl player)
    {
        print("Player " + player.playerNum + " score:" + player.getScore());
        return player.getScore() >= value;
    }

	private void setCrateValues() {
		float rando = Random.Range (0f, 1f);
		if (rando < .33) {
			drop = drop1;
			powerupText = "Armor";
		} else if (rando < .66) {
			drop = drop2;
			powerupText = "Boots";
		} else {
			drop = drop3;
			powerupText = "Sword";
		}

		value = (int) Random.Range (50, 100);

	}
}
