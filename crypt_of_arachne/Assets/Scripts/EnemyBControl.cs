﻿using UnityEngine;
using System.Collections;

public class EnemyBControl : MonoBehaviour
{
	GameManager gameManager;

	// the coin prefab
	public GameObject drop;

	// the powerup prefabs
	public GameObject pwrHealth;
	public GameObject pwrSpeed;
	public GameObject pwrDamage;
	public GameObject pwrInvul;

	Level2WaveManager l2wm;
	Level1WaveManager l1wm;
    public float speed;

    private float boundRadius;
    private float screenRatio;
    private float widthOrtho;

    private Vector3 targetPos;

    public GameObject flame;

    public float distance;

	public int health;

	private bool hit;
	private Rigidbody2D rb;

	public float knockBackStrength;


    // Use this for initialization
    void Start()
    {
        boundRadius = gameObject.GetComponent<CircleCollider2D>().radius;
        screenRatio = (float)Screen.width / (float)Screen.height;
        widthOrtho = Camera.main.orthographicSize * screenRatio;

        float x = Random.Range(-widthOrtho + 4f, widthOrtho - boundRadius - 4f);
        float y = Random.Range(-Camera.main.orthographicSize + 3.5f, Camera.main.orthographicSize - boundRadius - 3.5f);

        targetPos = new Vector3(x, y, 0f);

		rb = GetComponent<Rigidbody2D> ();

		gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
		if (GameObject.Find("WaveManager")) {
			//later on we'll have different wave managers for different levels
			if (Application.loadedLevelName == "Game")
				l1wm = GameObject.Find("WaveManager").GetComponent<Level1WaveManager>();
			else if (Application.loadedLevelName == "level_2")
				l2wm = GameObject.Find("WaveManager").GetComponent<Level2WaveManager>();
		}
    }

	void FixedUpdate () {
		if (hit) {
			rb.isKinematic = false;
			rb.WakeUp ();
			Vector3 direction = (targetPos - transform.position).normalized;
			rb.AddForce (-direction * knockBackStrength, ForceMode2D.Impulse);
			//rb.isKinematic = true;
			hit = false;
		}
	}

    // Update is called once per frame
    void Update()
    {
        distance = Vector2.Distance(transform.position, targetPos);

		if (distance < 2) {
			speed += 0.005f;
		}

        if (distance > 0.1)
        {
            transform.position = Vector3.Slerp(transform.position, targetPos, speed);
        }
        else
        {
			Instantiate(flame, transform.position, transform.rotation);
            Destroy(gameObject);
			if (l2wm != null) {
				l2wm.enemyDown ();
			} else if (l1wm != null) {
				l1wm.enemyDown ();
			}
        }

		if (health <= 0) {
			die ();
		}
    }

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Sword" || other.tag == "EnemyAttack") {
			//Check that this isn't our own attack
			GameObject otherParent = other.gameObject.transform.parent.gameObject;
			if (otherParent == null || otherParent != this.gameObject) {
				AttackLength attack = other.GetComponent<AttackLength> ();
				//If the spider collides with an attack object that's only been onscreen for 
				//<= .05 seconds, the spider dies
				if (attack != null && attack.getTimer () <= .05f) {
					hit = true;
					health -= otherParent.GetComponent<PlayerControl> ().damage;
				}
			}
		}
	}

	//Kills spider object and drops gold.
	public void die()
	{
		float pChance = Random.Range (0f,1f);
		//Spiders have a 90% chance to drop gold on death and a 10% chance to drop a powerup
		if (pChance > 0.1) {
			Instantiate (drop, transform.position, transform.rotation);
			int coindrop = Mathf.CeilToInt(Random.Range(5f,10f));
			for (int i = 0; i < coindrop; i++) {
				Vector2 temp_v = new Vector2(Random.Range(-1f,1f),Random.Range(-1f,1f));
				float temp_speed = Random.Range (1f, 5f);
				GameObject temp = Instantiate(drop, transform.position,transform.rotation) as GameObject;
				Rigidbody2D temp_rb = temp.GetComponent<Rigidbody2D> ();
				temp_rb.AddRelativeForce (temp_v * temp_speed, ForceMode2D.Impulse);
			}
		} else {
			//each powerup is equally likely to drop
			//yeah this code is going to make somebody pretty angry once we add more than 4 powerups
			if (pChance < 0.025)
				Instantiate (pwrDamage, transform.position, transform.rotation);
			else if (pChance < 0.05)
				Instantiate (pwrHealth, transform.position, transform.rotation);
			else if (pChance < 0.075)
				Instantiate (pwrInvul, transform.position, transform.rotation);
			else 
				Instantiate (pwrSpeed, transform.position, transform.rotation);
		}
		if (gameManager.isWaveLevel())
		{
			if (l2wm != null) {
				l2wm.enemyDown ();
			} else if (l1wm != null) {
				l1wm.enemyDown ();
			}
		}
		Destroy(gameObject);
	}
}