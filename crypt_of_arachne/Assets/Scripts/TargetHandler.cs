﻿using UnityEngine;
using System.Collections;

public class TargetHandler : MonoBehaviour {

	GameManager gameManager;
	private int playerNum;
	private PlayerControl target;

	private GameObject tempTarget;

	// Use this for initialization
	void Start () {
		gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
		playerNum = Random.Range(1, 5);
		target = gameManager.getPlayer (playerNum);
	}
	
	// Update is called once per frame
	void Update () {

		if (target == null || !target.active)
        {
            playerNum = Random.Range(1, 5);
            target = gameManager.getPlayer(playerNum);
        }
		
	}

	public PlayerControl getTarget() {
		return target;
	}

	public void setTempTarget(GameObject t) {
		tempTarget = t;
	}

	public GameObject getTempTarget() {
		return tempTarget;
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Player") {
			target = gameManager.getPlayer(other.GetComponent<PlayerControl> ().playerNum);
		}
	}
}
