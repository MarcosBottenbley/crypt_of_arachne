﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class WinScript : MonoBehaviour
{
    GameManager gm;

    // Use this for initialization
    void Start()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        gameObject.GetComponent<Text>().text = scores(gm.getScores());

        destroyPlayers();
    }

    // Update is called once per frame
    void Update()
    {
    }

    private string scores(float[] scor)
    {
        print("sorting scores");

        List<KeyValuePair<int, float>> d = new List<KeyValuePair<int, float>>();
        for (int k = 1; k < 5; k++)
        {
            d.Add(new KeyValuePair<int, float>(k, scor[k - 1]));
        }

        d.Sort(
            delegate(KeyValuePair<int, float> pair1,
            KeyValuePair<int, float> pair2)
            {
                return pair2.Value.CompareTo(pair1.Value);
            }
        );

        string s = "";
        for (int i = 0; i < d.Count; i++)
        {
            s += "Player " + d[i].Key + " - " + d[i].Value + "\n";
        }
        return s;
    }

    private void destroyPlayers()
    {
        for (int i = 1; i <= 4; i++)
        {
            Destroy(GameObject.Find("Player" + i));
        }
        Destroy(GameObject.Find("Players"));
    }
}