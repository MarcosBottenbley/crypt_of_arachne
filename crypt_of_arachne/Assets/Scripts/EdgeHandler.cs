﻿using UnityEngine;
using System.Collections;

public class EdgeHandler : MonoBehaviour {

	private float boundRadius;
	private float screenRatio;
	private float widthOrtho;
	private Vector3 pos;

	private Rigidbody2D rb;

	// Use this for initialization
	void Start () {
		boundRadius = gameObject.GetComponent<CircleCollider2D> ().radius;
		screenRatio = (float) Screen.width / (float) Screen.height;
		widthOrtho = Camera.main.orthographicSize * screenRatio;

		rb = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		pos = transform.position;

		//top - top
		//bottom - bottom
		if (pos.y + boundRadius > Camera.main.orthographicSize) {
			pos.y = Camera.main.orthographicSize - boundRadius;
			transform.position = pos;

			if (rb.velocity.magnitude != 0) {
				rb.velocity = -rb.velocity;
			}

		} else if (pos.y - boundRadius < -Camera.main.orthographicSize) {
			pos.y = -Camera.main.orthographicSize + boundRadius;
			transform.position = pos;

			if (rb.velocity.magnitude != 0) {
				rb.velocity = -rb.velocity;
			}
		}

		//top - right
		//bottom - left
		if (pos.x + boundRadius > widthOrtho) {
			pos.x = widthOrtho - boundRadius;
			transform.position = pos;

			if (rb.velocity.magnitude != 0) {
				rb.velocity = -rb.velocity;
			}

		} else if (pos.x - boundRadius < -widthOrtho) {
			pos.x = -widthOrtho + boundRadius;
			transform.position = pos;

			if (rb.velocity.magnitude != 0) {
				rb.velocity = -rb.velocity;
			}
		}
	}
}
