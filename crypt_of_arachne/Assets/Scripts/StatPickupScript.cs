﻿using UnityEngine;
using System.Collections;

public class StatPickupScript : MonoBehaviour {

	//enum for types of statups. uses ints starting from 0, same as
	//the one in PlayerControl
	private enum statups {Health, Speed, Damage};
	//what type of statup this is (set in editor)
	public int sType;

	private AudioSource sfx;

	// Use this for initialization
	void Start () {
		sfx = gameObject.GetComponent<AudioSource>();
	}

	// Update is called once per frame
	void Update () {

	}

	void OnCollisionEnter2D(Collision2D coll) 
	{
		PlayerControl player = coll.gameObject.GetComponent<PlayerControl>();
		if (player.gameObject.activeSelf)
		{
			sfx.Play();
			player.getStatUp (sType);
		}
		Destroy(gameObject);
	}
}
