﻿using UnityEngine;
using System.Collections;

public class FacePlayer : MonoBehaviour {
	
	private Transform target;
	private Vector2 v_diff;
	private float atan2;
	public GameObject targetHandler;
	private TargetHandler th;

	// Use this for initialization
	void Start () {
		th = targetHandler.GetComponent<TargetHandler> ();
		target = th.getTarget().transform;
	}
	
	// Update is called once per frame
	void Update () {
		v_diff = (target.position - transform.position);
		atan2 = Mathf.Atan2(v_diff.y, v_diff.x);
		transform.rotation = Quaternion.Euler(0f,0f, atan2 * Mathf.Rad2Deg);
	}
}
