﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EndScript : MonoBehaviour
{
    GameManager gm;

    // Use this for initialization
    void Start()
    {
        Destroy(GameObject.Find("GameManager"));
        destroyPlayers();
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void destroyPlayers()
    {
        for (int i = 1; i <= 4; i++)
        {
            Destroy(GameObject.Find("Player" + i));
        }
        Destroy(GameObject.Find("Players"));
    }
}