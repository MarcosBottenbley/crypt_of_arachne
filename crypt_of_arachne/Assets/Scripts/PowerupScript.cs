﻿using UnityEngine;
using System.Collections;

public class PowerupScript : MonoBehaviour {

	//enum for types of powerups. uses ints starting from 0, same as
	//the one in PlayerControl except this one has Health mapped to 0
	//instead of Nothing
	private enum powerups {Health, Speed, Damage, Invul};
	//what type of powerup this is (set in editor)
	public int pType;

    private AudioSource sfx;

	// Use this for initialization
	void Start () {
        sfx = gameObject.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D coll) 
	{
		PlayerControl player = coll.gameObject.GetComponent<PlayerControl>();
		if (player.gameObject.activeSelf)
		{
            sfx.Play();
			if (pType == (int)powerups.Health) {
				player.healthUp (2);
			} else {
				player.getPowerup (pType);
			}
		}
		Destroy(gameObject);
	}
}
