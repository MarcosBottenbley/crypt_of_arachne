﻿using UnityEngine;
using System.Collections;

public class SpiderControl : MonoBehaviour {

    GameManager gameManager;
	Level1WaveManager l1wm;
    public float speed;

	private Rigidbody2D rb;
	private float distance;
	private Vector3 direction;

	public float minDistance;

    // the player to target
    private int playerNum;

    // the coin prefab
    public GameObject drop;

	// the powerup prefabs
	public GameObject pwrHealth;
	public GameObject pwrSpeed;
	public GameObject pwrDamage;
	public GameObject pwrInvul;

	public int health;
	public int damage;

	public float knockBackStrength;

	private PlayerControl target;
	private GameObject tempTarget;

	public GameObject targetHandler;
	private TargetHandler th;

	private Vector2 v_diff;
	private float atan2;

	public float attackTimer;
	private bool attacking;

	private bool hit;
	public GameObject attackPrefab;
	private GameObject attack;

	public bool invulverable;

	private Color currentColor;
	private SpriteRenderer sr;

	private bool otherTarget;
	// Use this for initialization
	void Start () {
		targetHandler = Instantiate(targetHandler, transform.position, transform.rotation) as GameObject;
		th = targetHandler.GetComponent<TargetHandler> ();
		rb = gameObject.GetComponent<Rigidbody2D> ();
		gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
		if (GameObject.Find("WaveManager")) {
			//later on we'll have different wave managers for different levels
			l1wm = GameObject.Find("WaveManager").GetComponent<Level1WaveManager>();
		}
		sr = GetComponent<SpriteRenderer> ();
		currentColor = sr.color;

		damage = 1;
	}

	void FixedUpdate () {
		if (hit) {
			rb.isKinematic = false;
			rb.WakeUp ();
			direction = (target.transform.position - transform.position).normalized;
			rb.AddForce (-direction * knockBackStrength, ForceMode2D.Impulse);
			//rb.isKinematic = true;
			hit = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
		targetHandler.transform.parent = transform;

		if (!otherTarget) {
			target = th.getTarget ();
		}

		distance = Vector2.Distance (transform.position, target.transform.position);
		//direction = target.transform.position - transform.position;

		if (otherTarget) {
			
			transform.position = Vector3.Lerp (transform.position, tempTarget.transform.position, speed);
			v_diff = (tempTarget.transform.position - transform.position);

		} else if (distance > minDistance && !hit) {
			
			v_diff = (target.transform.position - transform.position);
			transform.position = Vector3.Lerp (transform.position, target.getPosition (), speed);

		} else {
            rb.velocity = Vector3.zero;
            rb.angularVelocity = 0;
			if (attackTimer > 0) {
				attackTimer -= Time.deltaTime;
			}

			if (attackTimer <= 0) {
				attackTimer = 3f;
				attack = Instantiate(attackPrefab, transform.position + transform.up, transform.rotation) as GameObject;
				//Sets the enemy attack's parent transform to this spider's transform.
				attack.transform.parent = this.gameObject.transform;
			}
		}

		atan2 = Mathf.Atan2 (v_diff.y, v_diff.x);
		transform.localEulerAngles = new Vector3(0f, 0f, atan2 * Mathf.Rad2Deg - 90);

		if (health <= 0) {
			die ();
		}
	}
		
	void OnTriggerEnter2D(Collider2D other) {

		if (!invulverable) {
			if (other.tag == "Sword" || other.tag == "EnemyAttack") {
				//Check that this isn't our own attack
				GameObject otherParent = other.gameObject.transform.parent.gameObject;
				if (otherParent == null || otherParent != this.gameObject) {
					AttackLength attack = other.GetComponent<AttackLength> ();
					//If the spider collides with an attack object that's only been onscreen for 
					//<= .05 seconds, the spider dies
					if (attack != null && attack.getTimer () <= .05f) {
						hit = true;
						health -= otherParent.GetComponent<PlayerControl> ().damage;
						sr.color = Color.red;
					}
				}
			}
		}
	}

	void OnTriggerExit2D(Collider2D other) {
		if (!invulverable) {
			if (other.tag == "Sword" || other.tag == "EnemyAttack") {
				sr.color = currentColor;
			}
		}
	}

	public void setTarget(GameObject t) {
		tempTarget = t;
		otherTarget = true;
	}

	private Vector2 knockBack() {
		float y = knockBackStrength * Mathf.Sin (target.getRotationDegrees());
		float x = knockBackStrength * Mathf.Cos (target.getRotationDegrees ());

		return new Vector2 (x, y);
	}

	//Kills spider object and drops gold.
	public void die()
	{
		float pChance = Random.Range (0f,1f);
		//Spiders have a 90% chance to drop gold on death and a 10% chance to drop a powerup
		if (pChance > 0.1) {
			Instantiate (drop, transform.position, transform.rotation);
			int coindrop = Mathf.CeilToInt(Random.Range(5f,10f));
			for (int i = 0; i < coindrop; i++) {
				Vector2 temp_v = new Vector2(Random.Range(-1f,1f),Random.Range(-1f,1f));
				float temp_speed = Random.Range (1f, 5f);
				GameObject temp = Instantiate(drop, transform.position,transform.rotation) as GameObject;
				Rigidbody2D temp_rb = temp.GetComponent<Rigidbody2D> ();
				temp_rb.AddRelativeForce (temp_v * temp_speed, ForceMode2D.Impulse);
			}
		} else {
			//each powerup is equally likely to drop
			//yeah this code is going to make somebody pretty angry once we add more than 4 powerups
			if (pChance < 0.025)
				Instantiate (pwrDamage, transform.position, transform.rotation);
			else if (pChance < 0.05)
				Instantiate (pwrHealth, transform.position, transform.rotation);
			else if (pChance < 0.075)
				Instantiate (pwrInvul, transform.position, transform.rotation);
			else 
				Instantiate (pwrSpeed, transform.position, transform.rotation);
		}
		if (gameManager.isWaveLevel())
		{
			l1wm.enemyDown();
		}
		Destroy(gameObject);
	}
}
