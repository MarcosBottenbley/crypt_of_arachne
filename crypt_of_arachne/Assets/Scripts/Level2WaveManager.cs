﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using CorruptedSmileStudio.Spawn;

public class Level2WaveManager : MonoBehaviour {

	private List<GameObject> spawners = new List<GameObject>();
	public GameObject spawner;
	private Spawner spawnScript;

	GameManager gameManager;

	//Number of enemies currently alive in the wave.
	private int numEnemies;

	private int currentWave;

	public GameObject wTextObj;
	private Text waveText;
	public Color waveTextColor;

	//Are we showing the wave indicator text right now?
	private bool showWaveText;
	//Tracks how long the wave indicator text stays onscreen.
	private float waveTextTimer;

	public int numSpawners;

	// Use this for initialization
	void Start () {
		gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
		spawnScript = spawner.GetComponent<Spawner> ();

		if (wTextObj) {
			waveText = wTextObj.GetComponent<Text> ();
		}

		waveTextColor = new Color (255, 255, 255, 255);

		//Initializes the wave text by setting the current wave to 1
		//and showWaveText to true (with the timer set at 2)
		currentWave = 0;
		changeWave ();
	}

	// Update is called once per frame
	void Update () {
		//This is supposed to fade out the wave text but it just
		//makes it disappear after a couple of seconds.
		if (showWaveText) {
			waveTextTimer -= Time.deltaTime;
			waveTextColor [3] = ((float) (255 / 2)) * waveTextTimer;
		}
		if (waveTextTimer <= 0) {
			showWaveText = false;
			waveTextColor [3] = 0;
		}
		waveText.color = waveTextColor;
	}

	private void startWave() {
		numEnemies = 0;
		for (int i = 0; i < 6; i++) {
			if (currentWave == 5 && i <= 6) {
				spawnScript.unitLevel = UnitLevels.Medium;
			} else {
				spawnScript.unitLevel = UnitLevels.Easy;
			}
			spawners.Add ((GameObject)Instantiate (spawner, setRandomLocation(10,6), Quaternion.identity)); 
			//magic number
			numEnemies = numEnemies + spawnScript.totalUnits;
		}
	}

	//Changes the wave text and starts the timer to fade it out.
	private void changeWave() {
		if (currentWave < 5) {
			currentWave = currentWave + 1;
			waveText.text = "Wave " + currentWave;
			showWaveText = true;
			waveTextTimer = 2f;
			numSpawners += 2;
			startWave ();
		} else {
			waveText.text = "Boss Incoming";
			showWaveText = true;
			waveTextTimer = 2f;
			gameManager.loadBoss ();
		}
	}

	public void enemyDown()
	{
		numEnemies = numEnemies - 1;
		//Changes the wave text once all the enemies in a wave are dead
		if (numEnemies <= 0) {
			clearSpawners ();
			changeWave ();
		}
	}

	private void clearSpawners() {
		for (int i = 0; i < 6; i++) {
			Destroy (spawners [i]);
		}
		spawners.Clear ();
	}

	// Returns a random location on the specified rectangle oriented around the origin.
	// x_rad = half the length of the rect, y_rad = half the height
	// (also this is terrible code, somebody who knows unity/csharp 
	// random functions better than me please edit this)
	private Vector3 setRandomLocation(float x_rad, float y_rad)
	{
		float x, y;
		float test = Random.value;
		if (test > 0.5) {
			x = Random.Range (x_rad * (-1), x_rad);
			test = Random.value;
			if (test > 0.5) {
				y = y_rad;
			} else {
				y = y_rad * (-1);
			}
		} else {
			y = Random.Range (y_rad * (-1), y_rad);
			test = Random.value;
			if (test > 0.5) {
				x = x_rad;
			} else {
				x = x_rad * (-1);
			}
		}
		Vector3 loc = new Vector3 (x, y, 0);
		return loc;
	}
}
