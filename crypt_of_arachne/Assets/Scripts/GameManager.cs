﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager instance = null;

	private List<PlayerControl> players = new List<PlayerControl>();
	private float[] playerTimers;
	private static float resTime = 5f;

    private float[] scores;

    // Is this a wave level?
    public bool waveLevel;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    void OnLevelWasLoaded()
    {
        players.Clear();
        for (int i = 1; i < 5; i++)
        {
            players.Add(GameObject.Find("Player" + i.ToString()).GetComponent<PlayerControl>());
        }
    }

	// Use this for initialization
	void Start () {
		// Grab the players from the scene
		for (int i = 1; i < 5; i++) {
			players.Add(GameObject.Find("Player" + i.ToString()).GetComponent<PlayerControl>());
		}
		playerTimers = new float[] {resTime,resTime,resTime,resTime};

        scores = new float[] { 0, 0, 0, 0 };
	}

	// Update is called once per frame
	void Update()
	{
		if (waveLevel) {
			for (int i = 0; i < 5; i++) {
				if (!players [i].isActiveAndEnabled && playerTimers [i] > 0) {
					playerTimers [i] -= Time.deltaTime;
					players [i].scoreDisplay.text = "Respawn In: " + Mathf.CeilToInt (playerTimers [i]);
				} else if (playerTimers [i] <= 0) {
					players [i].gameObject.SetActive (true);
					players [i].respawn ();
					playerTimers [i] = resTime;
				}
			}
		}

		// if all ded
        bool notDed = false;
        for (int i = 0; i < 4; i++)
        {
            notDed = notDed || players[i].isActiveAndEnabled;
        }
        if (!notDed)
        {
            SceneManager.LoadScene("Death");
        }
	}

	public PlayerControl getPlayer(int i)
	{
		return players[i-1];
	}

	public void loadBoss() {
		waveLevel = false;
		StartCoroutine (startBoss());
	}

	IEnumerator startBoss() {
		yield return new WaitForSeconds (3);
		SceneManager.LoadScene ("Boss");
	}

	public void loadLevel2() {
		StartCoroutine (startLevel2 ());
	}

	IEnumerator startLevel2() {
		yield return new WaitForSeconds (3);
		SceneManager.LoadScene ("level_2");
	}

	public List<PlayerControl> getPlayers()
    {
        return players;
    }

	public bool isWaveLevel()
	{
		return waveLevel;
	}

    public void addGold(int player, float amount)
    {
        scores[player - 1] += amount;
    }

    public float getScore(int playerNum)
    {
        return scores[playerNum - 1];
    }

    public float[] getScores()
    {
        return scores;
    }
}