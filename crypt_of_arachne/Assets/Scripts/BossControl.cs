﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class BossControl : MonoBehaviour {

    // the health of the boss
    public int health;

    // the prefab the boss spawns
    public GameObject spawnObject;

    // the amount of enemies to spawn when boss disappears
    public int spawnAmount;

    // the amount of time the boss appears
    public int visibleTime;

	public int fadeTime;

    // the amount of time the boss is hidden
    public int hiddenTime;

    // whether or not the boss is visible
    private bool isVisible;

    // the list of all objects in the spawn
    // private ArrayList spawn;

    // timer to keep track of time boss is visible/hidden
    public float timer;

	public float fadeTimer;

	private List<GameObject> spiders;
	public float minDistance;

    // the boss's sprite
    private SpriteRenderer sprite;

	private bool destroyed;

	// Use this for initialization
	void Start () {
        isVisible = true;
		destroyed = false;
        timer = 2f;
        // spawn = new ArrayList();

        sprite = gameObject.GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {

        if (health <= 0) {
            die();
        }

        // handle events when boss is invisible
        if (!isVisible)
        {
            // decrease timer
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
				if (destroyed) {
					timer = visibleTime;
					isVisible = true;


					// show the sprite
					//sprite.color = new Color (1f, 1f, 1f, 1f);
				} else if (spidersGathered ()) {
					destroySpawn ();
					//sprite.color = new Color(1f, 1f, 1f, 1f);
					fadeTimer = fadeTime;
				} else {
					gatherSpiders ();
				}
            }
        }
        // handle events when boss is visible
        else
        {
            // make sure sprite is visible
			sprite.color = new Color (1f, 1f, 1f, Mathf.SmoothStep (1f, 0f, fadeTimer));

            timer -= Time.deltaTime;
			fadeTimer -= Time.deltaTime;
            if (timer <= 0)
            {
                timer = hiddenTime;
                isVisible = false;

                createSpawn();
                moveBoss();
                
                // hide the sprite
                sprite.color = new Color(1f, 1f, 1f, 0f);
            }
        }
	}

    // creates the spawn of enemies
    public void createSpawn()
    {
		spiders = new List<GameObject> ();
        Vector3 position = gameObject.transform.position;
        for (int i = 0; i < spawnAmount; i++)
        {
			GameObject temp;
			SpiderControl tempsp;
            // spawn.Add(Instantiate(spawnObject, position, new Quaternion()));
			temp = Instantiate(spawnObject, position, new Quaternion()) as GameObject;
			tempsp = temp.GetComponent<SpiderControl> ();
			tempsp.speed = 0.02f;
			tempsp.health = 4;
			spiders.Add (temp);
        }
		destroyed = false;
    }

    // destroys the spawn of enemies
    private void destroySpawn()
    {
		foreach (GameObject spider in spiders) {
			Destroy (spider);
		}
		destroyed = true;
    }

	private void gatherSpiders() {
		foreach (GameObject spider in spiders) {
			spider.GetComponent<SpiderControl> ().setTarget (gameObject);
		}
	}

	private bool spidersGathered() {

		if (destroyed) {
			return true;
		}

		bool gathered = false;
		foreach (GameObject spider in spiders) {
			float distance = Vector2.Distance (transform.position, spider.transform.position);
			if (distance <= minDistance) {
				gathered = true; 
			} else { 
				gathered = false;
			}
		}

		return gathered;
	}

    private void moveBoss()
    {
        float x = Random.Range(-5, 5);
        float y = Random.Range(-2, 2);

        gameObject.transform.position = new Vector3(x, y, 0);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
		if (!isVisible) {
			AttackLength attack = other.GetComponent<AttackLength>();
			//If the spider collides with an attack object that's only been onscreen for 
			//<= .05 seconds, the spider dies
			if (attack != null && attack.getTimer() <= .05f)
			{
				// rb.AddForce(knockBack());
				health -= 1;
			}
		}
    }

    // kills boss and drops something possibly
    public void die()
    {
        // Instantiate(drop, transform.position, transform.rotation);
        Destroy(gameObject);
        SceneManager.LoadScene("Win");
    }
}
