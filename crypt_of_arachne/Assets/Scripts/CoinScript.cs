﻿using UnityEngine;
using System.Collections;

public class CoinScript : MonoBehaviour {

    // min and max values of coins
    public int minValue;
    public int maxValue;

    // ranged value assigned
    public int value;

	// Use this for initialization
	void Start () {
		value = 1;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter2D(Collision2D coll) 
    {
        PlayerControl player = coll.gameObject.GetComponent<PlayerControl>();
        if (player.gameObject.activeSelf)
        {
            player.addGold(value);
        }
        Destroy(gameObject);
    }

    public void setValue(int v)
    {
        print("setting value " + v);
        value = v;
        minValue = v;
        maxValue = v;
    }
}
