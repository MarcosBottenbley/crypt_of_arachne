﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using CnControls;

public class PlayerControl : MonoBehaviour {

    //public Joystick joystick;		deprecated code for old joystick library
    public Button attackButton;
    public Text scoreDisplay;

    private float score;

    public int playerNum;
	public float speed;

	private float attackTimer;
	private bool attacking;
	private float chargeTimer;
	private bool charging;

	private float powerupTimer;
	//How long a powerup lasts
	private float powerupLength;
	//Enum to track powerup values - uses ints (0,1,2,3)
	private enum powerups {Nothing, Speed, Damage, Invul};
	//The powerup (in the powerups enum) that is currently active
	private int currentPowerup;

	//Enum to track stat-increasing items; works the same as powerup one
	private enum statups {Health, Speed, Damage};

	//Amount of time the player is unable to attack after attacking
	public float playerWaitTime;
	//Amount of time it takes to charge a spin attack
	public float chargeMaxTime;

	public GameObject attack;
	public GameObject spinAttack;

	//maximum health this player can have
	public int maxHealth;
	//the player's current health
	public int health;
	//damage dealt per hit
	//we should change this to a float, keeping it as int right now because it's more convenient
	public int damage;

    public GameObject drop;

    // fraction of gold lost on death
    public float goldLost;
	public bool active;

    // sfx and animation
    private AudioSource[] sfx;
    private Animator animator;

	//set position to respawn to
	public Vector3 respawnPosition;
	
    private GameManager gm;

	private GameObject healthBar;
	private healthBar hb;

	// Use this for initialization
	void Start () {

		//DontDestroyOnLoad(gameObject);

		respawnPosition = transform.position;

		speed = 2;
        score = 0;
		damage = 1;

		powerupLength = 10f;

		if (health != null && health != 0) {
			maxHealth = health;
		}

		attackTimer = 0;
		chargeTimer = 0;
		attacking = false;
		charging = false;
		playerWaitTime = .3f;
		chargeMaxTime = .5f;
        scoreDisplay.text = "HP: " + health + " GP: " + score;
		active = true;

        // sfx
        sfx = gameObject.GetComponents<AudioSource>();
        animator = this.GetComponent<Animator>();

        gm = GameObject.Find("GameManager").GetComponent<GameManager>();

		//healthBar = Instantiate(healthBar, transform.position, transform.rotation) as GameObject;
		//hb = healthBar.GetComponent<healthBar> ();
	}

	void OnLevelWasLoaded()
    {
        scoreDisplay = GameObject.Find("Score" + playerNum).GetComponent<Text>();
        attackButton = GameObject.Find("Attack" + playerNum).GetComponent<Button>();
    }
	
	// Update is called once per frame
	void Update () {

		//healthBar.transform.parent = transform;

        // displays the hp and gold
        scoreDisplay.text = "HP: " + health + " GP: " + gm.getScore(playerNum);

		// Creates a Vector2 to store the current joystick position.
		// With the library we're using now, this is handled by two calls to the "CnInputManager"
		// with arguments corresponding to the axes we want. Since we don't want one player's joystick
		// affecting the other players' movement, I made a separate horizontal and vertical axis in the 
		// Input Manager for each player. If there's a better way of doing this, feel free to change it.
		Vector2 movement = new Vector2(CnInputManager.GetAxis("Horizontal" + playerNum),
			CnInputManager.GetAxis("Vertical" + playerNum));

		// The player can't move while in "attacking" state
		if (!attacking && movement != Vector2.zero)
        {
            // print("Player " + playerNum + " joystick: " + joystick.JoystickInput);
			// Moves the player in the direction of the JoystickInput vector.
			transform.Translate(movement*Time.deltaTime*speed, Space.World);
			// Sets z-axis rotation to the angle the player is moving.
			float angle = Mathf.Atan2(movement.y, movement.x);
			angle = angle * (180 / Mathf.PI);
			transform.localEulerAngles = new Vector3(0,  0, angle - 90);
			// print (angle);

            // walking animation
            animator.SetInteger("State", 1);
        }

        if (movement == Vector2.zero && !attacking)
        {
            // idle animation
            animator.SetInteger("State", 0);
        }

		// increments the wait timer if the player is attacking
		if (attacking) {
			attackTimer = attackTimer + Time.deltaTime;

            // attacking animation
            animator.SetInteger("State", 2);
		}
		// Right now the amount of time the player has to wait is declared inside
		// this if statement because I'm a bad programmer
		if (attackTimer > playerWaitTime) {
			attacking = false;
			attackTimer = 0;
		}

		if (charging) {
			chargeTimer = chargeTimer + Time.deltaTime;
		}

		if (health <= 0) {
			die ();
		}

		if (currentPowerup != (int) powerups.Nothing) {
			if (powerupTimer > 0) {
				powerupTimer -= Time.deltaTime;
			} else {
				endPowerup (currentPowerup);
			}
		}
	}

    public Vector3 getPosition() {
        return transform.position;
    }

    public void addGold(int gold) {
        score += gold;
        gm.addGold(playerNum, gold);
        scoreDisplay.text = score + "";
    }

	//Create an attack object and enter "attacking" state.
	public void Attack() {
		//Instantiate attack object and set this player as parent
		GameObject atk = Instantiate(attack, transform.position + transform.up, transform.rotation) as GameObject;
		atk.transform.parent = this.gameObject.transform;
		attacking = true;

        // play sounds
        playSfx();
	}

	public void startCharge() {
		charging = true;
	}

	public float getRotationDegrees() {
		return transform.eulerAngles.z;
	}

	//Reset charge timer, create a spin attack object and enter "attacking" state.
	public void endCharge() {
		charging = false;
		if (chargeTimer > chargeMaxTime) {
			//Instantiate spin attack object and set this player as parent
			GameObject spAtk = Instantiate(spinAttack, transform.position, transform.rotation) as GameObject;
			spAtk.transform.parent = this.gameObject.transform;
			attacking = true;
		}
		chargeTimer = 0;
	}

	void OnTriggerEnter2D(Collider2D other) {
		//Check that we're colliding with an attack collider
		if (other.tag == "Sword" || other.tag == "EnemyAttack") {
			//Check that this isn't our own attack
			GameObject otherParent = other.gameObject.transform.parent.gameObject;
			if (otherParent == null || otherParent != this.gameObject) {
				AttackLength attack = other.GetComponent<AttackLength> ();
				//If the player collides with an attack object that's been onscreen for 
				//<= .05 seconds, the player takes damage corresponding to the attacking
				//enemy's damage value
				if (attack != null && attack.getTimer () <= .05f && currentPowerup != (int) powerups.Invul) {
					//This makes four nested if statements. Hopefully I think of something
					//better before I push this, but if not, remind me (David) to change it.
					//We'll also need to add more code as we add more enemies with this, which
					//seems a little unnecessary. I feel like we should probably start using inheritance
					//(if c# has that).
					int damage = 0;
					if (other.tag == "Sword")
						damage = otherParent.GetComponent<PlayerControl> ().damage;
					else if (other.tag == "EnemyAttack")
						damage = otherParent.GetComponent<SpiderControl> ().damage;

					health -= damage;
					//hb.loseHealth (damage);
				}
			}
		}
	}

	//Kills player and drops gold.
	public void die()
	{
		//List<GameObject> coins = new List<GameObject> ();
		int coindrop = Mathf.CeilToInt (score * goldLost);
		score -= coindrop;
        scoreDisplay.text = "HP: " + health + " GP: " + score;

        gameObject.SetActive(false);
		active = false;

		for (int i = 0; i < coindrop; i++) {
			Vector2 temp_v = new Vector2(Random.Range(-1f,1f),Random.Range(-1f,1f));
			float temp_speed = Random.Range (10f, 20f);
			GameObject temp = Instantiate(drop, transform.position,transform.rotation) as GameObject;
			Rigidbody2D temp_rb = temp.GetComponent<Rigidbody2D> ();
			temp_rb.AddRelativeForce (temp_v * temp_speed, ForceMode2D.Impulse);
		}

		/**
		coins = coins.OrderBy (x => x.GetComponent<CoinScript> ().value).ToList ();

		for (int i = 0; i <= coins.Count (); i++) {
			
		}
		*/
	}

	public void respawn() {
		transform.position = respawnPosition;
		active = true;
		//TODO: magic number, change this when we add in max health
		health = 10;
		//hb.resetHealth ();
	}

	public float getScore()
    {
        return score;
    }

    public void purchaseCrate(int value)
    {
        score -= value;
    }

    public void playSfx()
    {
        sfx[0].Play();
        if (Random.Range(0, 2) == 0)
            sfx[1].Play();
    }

	public void getPowerup(int i)
	{
		if (currentPowerup != (int) powerups.Nothing) {
			endPowerup (currentPowerup);
		}

		if (i == (int) powerups.Speed) {
			speed += 2;
		} else if (i == (int) powerups.Damage) {
			damage += 2;
		} else if (i == (int) powerups.Invul) {
			//invul == true
			//we don't actually need to do anything here but i'm keeping it for clarity
		}
		powerupTimer = powerupLength;
		currentPowerup = i;
	}

	public void endPowerup(int i) 
	{
		if (i == (int) powerups.Speed) {
			speed = speed - 1;
		} else if (i == (int) powerups.Damage) {
			damage -= 2;
		} else if (i == (int) powerups.Invul) {
			//invul == false
		}
		powerupTimer = powerupLength;
		currentPowerup = (int) powerups.Nothing;
	}

	public void getStatUp(int i) {
		if (i == (int) statups.Health) {
			maxHealth += 2;
		} else if (i == (int) statups.Speed) {
			speed += 1;
		} else if (i == (int) statups.Damage) {
			damage += 1;
		}
	}

	public void healthUp(int i) {
		health += i;
		if (health > maxHealth) {
			health = maxHealth;
			//hb.resetHealth();
		}
	}

}
