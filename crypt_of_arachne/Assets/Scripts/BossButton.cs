﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BossButton : MonoBehaviour
{

	GameManager gameManager;

	// Use this for initialization
    void Start()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(() => Restart());
		gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Restart()
    {
		gameManager.loadBoss ();
		gameObject.SetActive (false);
    }
}
