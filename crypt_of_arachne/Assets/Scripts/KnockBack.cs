﻿using UnityEngine;
using System.Collections;

public class KnockBack : MonoBehaviour {

	public Rigidbody rb;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.CompareTag("jabbing"))
		{

			rb.AddForce(Vector3.left * 3);
		}
	}
}
