﻿using UnityEngine;
using System.Collections;

//Script for destroying an attack after it's created.
//The attack/spin attack objects are triggers and are set to be 
//rendered behind everything else for now.
public class AttackLength : MonoBehaviour {

	//Timer for measuring how long the attack stays visible on screen.
	//Note: right now this doesn't actually affect how long it stays active, there's
	//code in SpiderControl that prevents anything from happening on
	//collision if the attack timer is at less than .15
	float timer;
	float endTimer = 0.5f;
	SpriteRenderer attackSprite;

    public int playerNum;

    private AudioSource[] sfx;

	// Use this for initialization
	void Start () {
		timer = 0f;
		attackSprite = gameObject.GetComponent<SpriteRenderer> ();

        sfx = gameObject.GetComponents<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		timer = timer + Time.deltaTime;

		//Fades out attack as timer increases.
		attackSprite.color = Color.Lerp (attackSprite.color, Color.clear, timer * 2);

		if (timer > endTimer) {
			Destroy (gameObject);
		}
	}

	//Return the timer value.
	public float getTimer() {
		return timer;
	}

    public void setPlayer(int p)
    {
        playerNum = p;
    }

    public int getPlayer()
    {
        return playerNum;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        for (int i = 0; i < sfx.Length; i++)
        {
            if (Random.Range(0, 2) == 0)
            {
                sfx[i].Play();
            }
        }
    }
}
